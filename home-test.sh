#!/usr/bin/env bash
# insecure registry ignores TLS handshake
minikube start --insecure-registry 0.0.0.0/0 
# allows us into the minikube docker env
eval $(minikube docker-env) 
# enables us to type kubectl instead of ...
alias kubectl='minikube kubectl --' 
#builds from local dir and tags it
docker build -t kong-oidc:2.4-oidc ./kongv3-dockerfile/home-version/
helm repo add kong https://charts.konghq.com
helm repo update
# helm chart version 2.9.1
helm install kong/kong --generate-name -f ./kongv4-helm-values/values.yaml
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install my-release bitnami/postgresql -f ./postgres/values.yaml 

#get into pg
#version of pg is low as both kong and konga has compatibility issues with newer versions
alias kubectl='minikube kubectl --'
export POSTGRES_PASSWORD=$(kubectl get secret --namespace default my-release-postgresql -o jsonpath="{.data.postgres-password}" | base64 -d)
kubectl run my-release-postgresql-client --rm --tty -i --restart='Never' --namespace default --image docker.io/bitnami/postgresql:11 --env="PGPASSWORD=$POSTGRES_PASSWORD" \
      --command -- psql --host my-release-postgresql -U postgres -d postgres -p 5432

#pg commands
echo
CREATE USER konga;
CREATE DATABASE konga OWNER konga;
ALTER USER konga WITH PASSWORD 'password';
exit

#install konga https://github.com/dangtrinhnt/konga-helm-chart
# helm install konga ./ -n kong -f ./konga/values.yaml
helm install konga ./konga/konga-helm-chart/ -n default -f ./konga/values.yaml

#spin up keycloak
kubectl apply -f ./keycloak/deploy-service.yaml
