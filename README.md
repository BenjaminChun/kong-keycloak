# Kong-OIDC on Minikube 

* [Explanation of folder and content](#explanation-of-folder-and-content)
* [Concept](#concept)
* [Tools](#tools)
* [Toy example](#toy-example)
* [Steps to deploy](#steps-to-deploy)
* [Alternative way](#alternatively-with-script)
    * [Note when setting up](#note-that-depending-on-the-helm-install-command-for-kong)
* [Challenges](#challenges)
* [Notes and Troubleshooting](#notes-and-troubleshooting)
    * [Error 443](#if-encounter-error-431--webpage-not-loading-when-testing-disable-full-scope-in-keycloak-this-will-reduce-the-cookie-size-and-as-a-result-header-size-which-then-allows-user-to-access)
    * [Error lua](#if-facing-lua-error)
    * [Error ssl](#if-encounter-ssl--tls-certificate-verify-fail)
* [Appendix](#appendix)
    * [Elab on tools](#more-info-on-tools)
    * [Journal](#journal-k8s-discovery)


## Explanation of folder and content
* `apps` = backend deployments and services. Includes httpbin and mockbin
* `archive`-kong = docker files, docker kompose files, dry-run, template kong deployments (failures from trials and error)
* `envoy` = deployment of envoy using helm and values.yaml
* `keycloak` = deployment and service of keycloak from k8s style declarative configuration
* `konga` = deploys konga and includes konga helm chart which isnt available on helm repo
* `kongv3-dockerfile` = contains dockerfile for constructing kong-oidc (third trial)
* `kongv4-helm-values` = helm values for creating kong (ingress controller and proxy)
* `operator` = new Directory for gateway operator and contains CRDs and guide on deploying operator
* `postgres` = helm values.yaml for deploying postgres for konga
* `.sh` files = scripts and test scripts for setting up toy example in work and home environment (with and without certs)

## Concept
- Kong set up with Konga for easy management
- OIDC plugin is added to Kong in order to communicate with Keycloak
- Backend/endpoint for Kong can be anything (mockbin or httpbin might be easier)

## Tools
[Minikube](https://minikube.sigs.k8s.io/docs/start/), [k9s](https://github.com/derailed/k9s/releases) (unpack from tar.gz), [helm](https://helm.sh/docs/intro/install/), [docker](https://docs.docker.com/desktop/install/linux-install/)
- More info on tools in [appendix](#appendix) section

## Toy example
![Toy example](./toy-example.png)

## Steps to deploy 
1. Edit values.yaml for Kong helm chart
2. Run [commands](https://artifacthub.io/packages/helm/kong/kong) for installing Kong using helm
```
helm repo add kong https://charts.konghq.com
helm repo update
helm install kong/kong --generate-name -f ./kongv4-helm-values/values.yaml
```
Useful [command](https://docs.konghq.com/kubernetes-ingress-controller/latest/deployment/minikube/) to get proxy ip
```
export PROXY_IP=$(minikube service -n kong kong-proxy --url | head -1)
echo $PROXY_IP
http://192.168.99.100:32728
```
3. [Install](https://github.com/bitnami/charts/tree/master/bitnami/postgresql) Postgresql using helm (prereq for Konga)
```
helm install my-release bitnami/postgresql -f ./postgres/values.yaml 
```
4. [Install](https://github.com/dangtrinhnt/konga-helm-chart) Konga using helm (need to clone konga repo first then refer to local helm chart)

Make changes to Postgres 
```
CREATE USER konga;
CREATE DATABASE konga OWNER konga;
ALTER USER konga WITH PASSWORD 'password';
```
```
helm install konga ./ -n kong -f ./konga/values.yaml
```
5. Spin up Keycloak and set up (using declarative config)
6. Link Konga to Kong admin (via konga UI)
7. Add services and routes via Konga or thru the [guide](https://docs.konghq.com/kubernetes-ingress-controller/latest/guides/using-oidc-plugin/)
8. Add plugin to the service via Konga using details found on Keycloak's wellknown
9. Test endpoints/routes to see if OIDC is involved
10. Look out for authorisation headers
11. Test out the signature verification (envoy's job)

## Alternatively (with script)
1. cd to kong-keycloak directory
2. Run script below
``` 
chmod +x ./deploy-toy-example.sh 
./deploy-toy-example.sh
```
3. Get IP using command below
```
export NODE_PORT=$(minikube kubectl -- get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services konga)
  export NODE_IP=$(minikube kubectl -- get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  echo http://$NODE_IP:$NODE_PORT
```
4. Get ports of services using k9s (access services like load balancers by finding the destination port, in other words, the trailing port number)
5. Continue from step 6 in section above.


## Note that depending on the helm install command for Kong
The pvc resource is not deleted even with helm uninstall
hence when we do not change the name of the new kong after we uninstalled the old kong with the same name.
Postgres will run into an error where the new password doesnt match with the old.

[solution](https://github.com/Kong/charts/blob/main/charts/kong/FAQs.md#kong-fails-to-start-after-helm-upgrade-when-postgres-is-used-what-do-i-do) : delete all past entries of kong-postgres

# Challenges
1. Setting up docker image for Kong-OIDC
    - Issues with certs due to xtraman network
    - Versioning issues
    * Solved with insertion of cert download in docker build file + minikube runs on insecure registry (work around)
    
2. Understanding routes and services and oidc
    * Services are reached through routes
    * Routes can be header based or path based
    - OIDC plugin has some confusing fields such as redirect uri
    * Redirect uri should ideally be an unavailable endpoint extended from the path in the route
    * Reason is that the endpoint will be treated as Kong's own so we will not be able to access the endpoint if set as a redirect uri under Kong's oidc

3. Implementing in k8s
    - Tried to use docker kompose
    * Effy didnt work as expected, everything is deployed as common/native k8s resources
    * Helm is a better alternative
    - Helm chart versioning issue to accommodate kong-oidc image and postgres
    - Helm chart versioning issue to accommodate konga and postgres
    * Check kong-oidc is built on which kong image and then match to helm chart's kong image
    * Check konga notes for compatible postgres version

4. Operator
    - Virtually no resources to teach the use of the kong/gateway operator
    * New development and only covers the setup of the operator
    - Introduced to new k8s api (gateway api)
    * Had to understand the api concept and how to use which includes why they came up with it and the history of gateways
    * [Referenced](https://www.youtube.com/watch?v=lCRuzWFJBO0&ab_channel=CNCF%5BCloudNativeComputingFoundation%5D) some use cases that would be applicable for future use when gateway api is implemented -> which operator will spin up and manage hopefully

# Notes and Troubleshooting
* kong-oidc:2.4-oidc is not a public available version/config of kong-oidc it is in fact done by shane/avery who provided the Dockerfile in the submodule in this repo.
(https://github.com/louketo/louketo-proxy/issues/652)
## if encounter error 431 / webpage not loading when testing, disable full scope in keycloak, this will reduce the cookie size and as a result header size which then allows user to access
* .wellknown path needs to be checked, many documentation do not have the correct path.
* these could be the cause of unexpected error
* if facing issues with permission denied: 
* try adding sudo in front
* to eliminate the hassle of above, refer to this https://docs.docker.com/engine/install/linux-postinstall/

## if facing lua error 
[solution](https://discuss.konghq.com/t/how-to-solve-the-init-by-lua-error-in-kong/7622)
## if encounter ssl / tls certificate verify fail
eg. error:1416F086:SSL routines:tls_process_server_certificate:certificate verify failed:ssl/statem/statem_clnt.c:1913:
add certificate to alpine by adding code segment from [link](https://devops.pages.xtraman.org/devops-docs/software-mirrors/getting-started/)


# Appendix
## More info on tools
- minikube
minikube version: v1.25.2
commit: 362d5fdc0a3dbee389b3d3f1034e8023e72bd3a7
- kubernetes in minikube
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.3", GitCommit:"816c97ab8cff8a1c72eccca1026f7820e93e0d25", GitTreeState:"clean", BuildDate:"2022-01-25T21:25:17Z", GoVersion:"go1.17.6", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.3", GitCommit:"816c97ab8cff8a1c72eccca1026f7820e93e0d25", GitTreeState:"clean", BuildDate:"2022-01-25T21:19:12Z", GoVersion:"go1.17.6", Compiler:"gc", Platform:"linux/amd64"}
- helm
version.BuildInfo{Version:"v3.9.0", GitCommit:"7ceeda6c585217a19a1131663d8cd1f7d641b2a7", GitTreeState:"clean", GoVersion:"go1.17.5"}
- docker images (kong, konga)
- kong base image --version = 2.8.1
- kong-oidc dockerfile from [reference](https://github.com/revomatico/docker-kong-oidc) albeit with some custom changes (insertion of certificates)
- current kong-oidc dockerfile used for this demo = kongv3/Dockerfile
- kong is set up using is using official kong/kong helm chart
- keycloak is set up is using simple k8 deployment and service files = keycloak/deploy-service.yaml
- konga is set up with helm chart [reference](https://github.com/dangtrinhnt/konga-helm-chart)
## Journal (K8s Discovery)
13/6: successfully able to set up mockbin on kubernetes, at least on dashboard its working but need to familiarise how to communicate with resources in minikube.
update that mockbin can be accessed from k8 using nodeport and then port forwarding(?) -> means that the image is built alright

14/6 tried to set up mockbin and ingress in k8. we got mockbin working, managed to pass their example but upon adding own service and deployment on both namespaces, unable to access thru kong routes. not sure why? error = 503 or 404
work on given examples using helm

15/6 no helm charts perhaps make one? work on a working yaml file first and finish by tdy? found that the kong image does not contain the plugin we have. so its a problem we prolly need to bring up the kong-oidc version onto kubernetes.
worked on boilerplate version, didnt seem to be working, many errors such as missing files and configs, pods not scheduling
working on new image proposed by avery
seems to working just like the prev version, minus some annoying errors, but the plugin is still not recognised so need to look for workaround or at least check with what they have currently.

16/6 check on csit config OR research on ways to add oidc, or print out the list of plugins, maybe my spelling wrong etc.
checked out csit config, found that they were using kong helm chart version 2.2.0 but upon trying to use the same version as them, ran into deprecated error need an older version of k8.
so gave up on replicating and tried the new version of kong's helm chart, matching kong version from base image to kong's helm chart.
set up of kong went well, but konga was problematic due to database issue, main error was "unknown message authenticationOk length 23" or smt. hence i removed all the db stuff from konga, meaning that i did a dry run and copied the manifest files into a local folder to declare using k8 directly. once konga was working, checked if able to connect to kong and then checked for plugins, oidc wasnt there, so this was an issue.
according to docker experience, there is a need to curl POST the plugin for it to work. tried that but didnt seem to work well.
now try to redo docker and trace back steps taken to do up oidc plugin, if possible wish to ignore konga. (manually add ingresses ourselves)

17/2 check with avery again on why mine isnt working, perhaps check if konga is really needed cuz there is an issue, maybe can ask him to take a look
steps taken for docker simulation:
1. build correct kong docker image with correct tag
2. run correct commmand sequence for composing kong -> kong-db then migrations x2 then kong and then the rest
3. link konga and kong tgt using konga UI 
4. set up services and routes in konga
5. set up client in keycloak
6. get the secrets and url from keycloak and prep the POST request using curl
7. run the POST request and check that the global plugin has been installed

big issue faced ytd was with postgres and konga integration, suspected version incompatibility, hinted from kong's helm template warning with postgres
ref https://github.com/pantsel/konga/pull/740
downgraded postgres and works fine
kong admin also shows oidc as enabled so now we figure out how to properly add the oidc plugin into kong
took hint from avery that oidc is configurable from konga ui side, searched again and found it lying under other plugins tab
had trouble adding plugin to service as database was not configured for kong
eventually solved it by configuring a postgres sub-chart(?) using values.yaml
once done then had trouble testing out the plugin with a testing backend
eventually understood more about the plugin

20/6 check with mentors on work done, ask to progress onto kong operator (2 versions so which and are they using any now?)
could update naming of kong-oidc version to prevent confusion
checked and seemed okay
could work on envoy but seems abit hard to deploy, not sure why there are so many variables and how to deploy
new operator guide didnt work, maybe shld look at old one or analyse whats wrong with the new one

22/6 somewhat working config of envoy
understood more about what parameters were in the yaml file
decided to go with the classic kubectl apply method instead of helm as helm includes many other stuff and not sure if its the correct envoy
seems like there is alot of variants and use cases for envoy, lets focus on envoyproxy and getting it to work for our usecase of jwt authentication
now we need to find out how envoy is connected to keycloak and or kong
current config is as a cluster ip, perhaps remove the service from backend and link them directly to envoy so that envoy can be the opening for that endpoint instead of itself.

23/6 work on envoy integrating with backend, kong and keycloak. perhaps look at operator if time permits or stuck without mentors arnd
envoy listener is he one listening to traffic. in my use case perhaps i shouldnt be using 0.0.0.0 as no traffic is being routed there?
working config of envoy, paired to various traffic, seems to be able to find services based on names, perhaps has something to do with TCP layer?
bumps here and there with jwt format, one of the headers did not follow jwt standard and since envoy was looking at the header, it got confused and denied entry.
disabling oidc seems to prevent access into envoy which means jwt auth in envoy is working as intended
however would like to try including headers into GET request to see if we can access site without going through keycloak portal
ref for envoy's jwt auth filter
https://www.envoyproxy.io/docs/envoy/latest/configuration/http/http_filters/jwt_authn_filter
recap on auth flow
client requests for endpoint from kong
kong redirects client to keycloak with redirect uri (kong rmbs which endpoint u were trying to access so after successful sign in, kong will bring u back, the redirect uri in konga is a domain that kong will consider its own so DONT put ur desired endpoint as that)
keycloak authenticates client passing auth code to client
client redirected back to kong with auth code and gives to kong
kong receives and trades with keycloak for auth token (jwt format) 
kong uses auth token to authorize user to access backend
backend is secured with envoy which does auth token introspection to confirm with authority that user is authorised to access endpoint before allowing
found out that dataplane is focused on forwarding messages or allowing traffic through. control plane is focused on deciphering and responding to traffic. rmb the eg of the soldier passing messages on vs if the message was intended for him
it is still unclear to me why there is a need to distinguish them

24/6 wfh, check on kong operator and work on it, take this time to rest up and prep for next week.
updated a home-version for script and relevant files

27/6 worked out envoy error, /mock endpoint didnt work, it was querying mockbin/mock which is an endpoint which doesnt exist. turns out that the prefix in envoy is being passed into the backend which is not what we thought, so we added a new field to rewrite the prefix so that the correct prefix is given
was looking into operators and was confused between kong operator and kong ingress controller
but if im not wrong, operator will deploy ingress controller and ingress/gateway. think of operator as the human controlling the deployment and stuff. then the ingress controller is the one executing the deployments and stuff. then the gateway is the legit loadbalancer that exposes ports. 
ingress controller = control plane
gateway = data plane
useful link that shares latest example of kong and also state of gateway operator [https://youtu.be/Zqlwn5TZknI?t=2015]
maybe we shld set a benchmark for what an operator is expected to do for the case of api gateways/proxies or even more specific to kong
off the top of my head:
 - self healing gateway/routes/ingress
 - act as a controller for the custom resources: kongingress,kongclusterplugin,kongconsumer,kongplugin

28/6 kong operator and mentor consult
example operator
 - deploying an application on demand
 - taking and restoring backups of that application's state
 - handling upgrades of the application code alongside related changes such as database schemas or extra configuration settings
 - publishing a Service to applications that don't support Kubernetes APIs to discover them
 - simulating failure in all or part of your cluster to test its resilience
 - choosing a leader for a distributed application without an internal member election process
kong operator -> custom resource manager -> declaratively spin up resources (yaml) -> using api calls might be another option
right now we are adding services and routes manually, what they want is maybe a yaml file that can do that for them
-> solution for that is already present in [https://docs.konghq.com/kubernetes-ingress-controller/latest/guides/getting-started/]
-> above example used ingress, but we know the existence of kongingress [https://docs.konghq.com/kubernetes-ingress-controller/latest/guides/using-kongingress-resource/] , kongingress adds on strip path functionality, and hash on ip(?)
so we know that there are ways to declare configs and kong will be able to fulfill even without the operator cuz the ingress-controller alr managing that
'''
echo "
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: demo
spec:
  ingressClassName: kong
  rules:
  - http:
      paths:
      - path: /foo
        pathType: ImplementationSpecific
        backend:
          service:
            name: echo
            port:
              number: 80
" | kubectl apply -f -
'''
seems like the original use of the operator has been outsourced to the ingress-controller? eg building ingresses
based on the youtube video, it seems like the gateway operator is meant to spin up gateways as we please
this website suggests that the gateway api will come together with kong's implementation of it. 

29/6 go and read thru documentation and produce smt for the kong-operator, perhaps some demo orr...

30/6 working on kong operator
found some useful links that might give insight to how to use the kong operator
relationship between the kong operator and the gateway api remains abit unclear to me
both seem to be work in progress for sure
but seems like the new way of deploying kong would require gateway apis to work
new found knowledge and insight on the kong operator from question asked on kubernetes slack with kong channel
kong operator is supposed to change the way we deploy (which is currently using the helm chart)
it is also able to work with the CRDs, maintain and update the versions of kong which seemed to be the manual portion of a human operator
refer to [operator-info.txt] for full transcript of question and answer

it was discovered that several changes must be done to the existing kong proxy and kong ingress controller in order for it to integrate with the gateway apis
[https://konghq.com/blog/kong-ingress-controller-2-2] [https://docs.konghq.com/kubernetes-ingress-controller/latest/guides/using-gateway-api/]
apparently only KIC (kong ingress controller version 2.2 survives the configuration)
updated KIC version from 1.3 to 2.3, not sure why was it 1.3 in the first place. 2.3 works with oidc use case as well as when using gateway apis
KIC version 2.2 and 2.3 failed at step where kind=gateway is deployed but not able to produce an IP address, instead status is waiting for controller.
their suggested work flow is to create an instance of kong-proxy and kong ingress controller, but with the appropriate tags on the ingress controller
check that ingress controller's logs show that the feature has been enabled, then move on to deploy the CRDs that are required for gateway api (done using the kustomize command [https://github.com/Kong/gateway-operator]) 
then deploy the gateway and gateway class and check the status of gateway, it should be able to produce an ip address for us
then we can deploy http route which matches to the service of deployment created and provides the path prefix for accessing it through kong

4/7 continue on kong operator, perhaps work on shell script to spin up the clusters
need to practise the clustering/namespace idea
reading up on clusters, virtual clusters and namespaces
doesnt seem like i know how to fix the issue about service accounts and cluster permissions
not sure why a service account cant access an external api -> online sources suggested namespace but it was clearly not looking for resources on a new namespace
gateway service account fixed with a work around lel
[https://kubernetes.io/docs/reference/access-authn-authz/rbac/]
'''
kubectl create clusterrolebinding serviceaccounts-cluster-admin \
  --clusterrole=cluster-admin \
  --group=system:serviceaccounts
'''

5/7 work with mentor and time to slides