# KONG OPERATOR / GATEWAY OPERATOR
### Goodbye Deprecated ~~Kong Operator~~ , Welcome Gateway Operator
Kong operator is deprecated,it is now called the gateway operator.

An **operator's goal** is to:
- Manage custom resources that vanilla kubernetes is unable to support.
- Another analogy is to think of how a human will manage the custom resources that k8s is unable to manage. the operator will seek to take the human's job away.

With regards to ingresses, kong and the gateway api community thinks that the ingress resource that kubernetes offers is lacking in some ways, hence they would like to bring in an operator and some custom resources to build on top of it.


### [Gateway-API](https://youtu.be/lCRuzWFJBO0)

Gateway APIs project defines Kubernetes APIs to configure advanced concepts like traffic splitting, header matching, and load balancing configuration.
- It provides new resources such as the gateway, gatewayclass, httproutes, udproutes etc.
- It keeps the definition of gateways and related resources standardised. Makes the format consistent (imagine learning one gateway and then wanting to change to another gateway and realising it is so different in implementation for k8s)
- This allows easier upgrades or migrations
- Helps with day 2 operations such as rolling out new versions of apps or testing new gateways

Introduced new resources **gateway** and **httproutes**
- Very loosely coupled, changing one component will not affect another
- Isolated from each other
- Devs configuring gateways will not have to touch httproutes and vice versa for devs for backend


### How are the operator and the new gatewayapi related?

Operator is built on the assumption that the gateway is built using the gateway api hence they are quite tightly coupled in that sense.

### Gateway Operator
- Goal
  - Manage custom resources (spin up and spin down of gateways)
  - Manage upgrades and lifecycle management
  - Enables backups and restoration
  - Mainly to integrate with new **Gateway-API**
- It is still unclear how the current implementation of the kong helm chart will integrate with the new implementation of operators. (ideal scenario is doing away with helm and just have the operator spin up its own gateways)
- However, we do know that they do not plan to change the way we configure routes and services, mentors are currently using the konga method, but we can also follow the online tutorial's method of declarative configuration (recommended method of spinning routes and services)
- The operator mainly wants to incoporate the gateway-api into the k8s system such that gateway kind is a native k8 type. this takes away the need to human's involvement in "managing the lifecycle" of the resources. (eg. automated upgrades and CRD-based configuration changes)

### WRT to mentors' questions regarding the operator + FAQ
1. Why is it not working 
    - Its in alpha, its still work in progress with several milestones away from achieving beta. it deploys without a problem, but it fails when finding the kong-proxy-service deployed by helm
2. What are the differences between the old and the new
    - Old one was stopped before v1, as soon as they realised limitations of helm
    - Switched over to golang
3. Whats next
    - Wait for the release of the operator
4. How will we benefit from using the operator
    - Allows for easier spinning up and down of gateways -> operator should be able to help us remove gateways and their plugins and routes cleanly (takes out the messiness of taking down gateway resource)
    - Easier management of multiple gateways
    - Integrated with the new gateway-api from k8s 
    - Helps to set up routes declaratively compared to using konga ui/admin api
    - Abstracts away the inner workings of kong, just need to spin this up and then there is a gateway ready for u to use -> no need to understand how k8s deploy the kong gateway or ingress controller, just need to know how to use the kong operator and add routes and plugins (user experience)
    - This will also act as the new way to spin up clusters, no need for helm anymore, so again users wun have to use additional tools just to work on kong
5. What features can we expect from it
    - Backups and restores
    - Automated upgrades
6. What does gateway api offer (basis of it also)
    - Provides consistency in the way gateway apis on k8s is set up and run (same user interface) -> even with different providers of gatewayclass or controller
    - Seamless transition between upgrades/migrations
    - Some [features](https://www.youtube.com/watch?v=lCRuzWFJBO0&ab_channel=CNCF%5BCloudNativeComputingFoundation%5D) , header matching
    - Traditional loadbalancer and ingress -> annotations
    - Where this shines is when we do a day 2
    - Rolling out a v2, imagine adding new routing as part of upgrade, dev wun need to interact with the gateway at all just talk to httproute -> imagine create new gateway but linked to same routes and services, just need to use diff gatewayclass the rest all same can le, it just works
    - Ingress scope is too small, not able to fit advanced concepts of gateways
    - Integrating with sig multi cluster

## Steps to use the operator
### Prereq
- Ingress controller and proxy must already be set up
### Steps
1. Install operator-related CRDs 

    ```kubectl apply -k "github.com/kubernetes-sigs/gateway-api/config/crd?ref=v0.4.3" # these are CRDs```

2. Upgrade helm with updated values.yaml file:

    ```ingressController.args.--feature-gates=Gateway=true```
    
    This tells the ingress controller to look out for the Gateway kind (integration)
#### next steps will **diverge** depending on source and file version
3. Deploy GatewayClass and Gateway using yaml
#### source 1 dated 28 Apr 22 - [link](https://www.youtube.com/watch?v=Zqlwn5TZknI&t=2015s)
- Set annotations to value "true" 
#### source 2 dated 24 Feb 22 - [link](https://youtu.be/-EdT3NAJ-1Y?t=366)
- Set annotations to value "true" 
#### source 3 no date - [guide](https://docs.konghq.com/kubernetes-ingress-controller/latest/guides/using-gateway-api/)
- Set annotation to value {namespace}/{kong-proxy-service} 

### And this is where it stops working
- After step 2, even though the features are enabled and the CRDs are recognised by the ingress controller, an unhelpful error 500 appears, stating "Kong API unexpected error".
- With step 3, the gateway is unable to produce IP addresses and status stays in waiting for controller.
- There isnt enough resources to continue from here, and since this is in alpha, it is expected to come with bugs, hence would not recommend using the operator in the near future until it is released.
- Also this operator relies on the gateway-api which is also work-in-progress so it might not be available anytime soon.

### Assuming gateway is able to connect to the helm deployed resources
```kubectl kustomize https://github.com/kong/gateway-operator/config/default | kubectl apply -f -```
- Install gateway-operator

## If run into serviceaccount permission issues
Run convenient but unsafe command
```
kubectl create clusterrolebinding serviceaccounts-cluster-admin \
  --clusterrole=cluster-admin \
  --group=system:serviceaccounts
```

# Conversation with Shane (Kong)
| Ben  | Shane (Kong)  |
|---|---|
| hello all, ive been curious about what the task of the gateway-operator is
traditionally, kubernetes operators are tasked to manage the custom resources of kong, eg. kongplugins, ingresses/services or routes in terms of kong
however, we can add plugins and ingresses successfully even without the operator, and it seems like this task is assigned to the ingress-controller.
so i would like to clarify what the kong-operator will aim to achieve |  | 
|  | The Gateway Operator will manage the lifecycle of Kong Gateways (and subsequently, the ingress controller) as native Kubernetes APIs. At a very high level (and you can see some examples of this in the README) the day zero with the Gateway Operator is to:<ul><li>Create a Gateway resource, and wait for it to have an IP address (or hostname)</li><li>Create an Ingress resource and start routing application traffic through the Gateway</li></ul>So in this way, you might think of it as a replacement for the Helm chart: like the helm chart it manages deploying both the gateway/proxy and the ingress controller for you, but with a multitude of potential features on top that we wouldn't get with the Helm chart, such as automated upgrades and CRD based configuration changes. <br/>To perhaps add a bit more illustration, the Gateway Operator will also be able to do things like dynamically create and manage the MTLS certificates between the ControlPlane (the ingress controller) and the DataPlane (the Kong Gateway (proxy)) and automatically clean up (this is part of what we mean when we say "managing the lifecycle") so that you don't have to do that manually as a human cluster operator.<br/>Things that a human operator used to have to do manually will become more (or in some cases perhaps fully) automated, but it's still using the components you already know like the ingress controller and the gateway under the hood. Let me know if that's helpful, or if you have any further questions about it!| 
| Nice! Thank you for the elaborate answer. So in other words, it will replace the current method of deploying the gateway (helm) and also handle upgrades and CRD changes automatically.<br/>But the current implementation of the ingress controller and gateway will remain the same.<ol><li>Will this also mean that the way we deploy our ingresses (services and routes) and custom plugins will remain the same? (eg. through declarative yaml files)</li><li>How will the operator change the way we deploy CRDs?</ol></li> |  |
|  | <ol><li>services and routes, and how you manage things like Ingress resources all remains practically the same</li><li>there will be new CRDs, for instance "Gateway" (which is actually going to be a native type like Ingress in time)</li></ol> |
| Thank you for your response<ol><li>How will the current implementation of helm chart integrate with the use of operators?</li><li>In this doc , we are shown the use of HTTPRoute to add a route to the service created. How does it differ from just an ingress resource? Will ingress be replaced?</li><li>How does the deprecated kong-operator compare to the new gateway-operator? Are there any differences in their goal? Or was it just a different implementation?</li><li>Could you also elaborate more on what you mean by managing the life cycle of the ingress controller or proxy?</li> | |
| Also, when i try to deploy the Gateway, its status stays in "waiting for controller", and i followed the guide from kong's official channel | |
| I have attached a logs of the ingress controller pod as there is a fishy error. the error 500 appears after installing through helm and setting feature-gates for gateway to true. environment is minikube. kong was set up using latest helm chart with the only override being the feature-gate gateway=true. guide followed was according to official channel video | |
| | `How will the current implementation of helm chart integrate with the use of operators?` At the moment we have no plans to integrate the current Helm chart. There's really nothing set in stone with this at this point, but a future where you don't use Helm at all is desired among maintainers. This is all open for discussion or feedback, so if you want to make a case for something I would recommend opening a discussion on the operator repo and make your case with fine details about reasoning/user-stories and acceptance criteria. |
| | `In this doc we are shown the use of HTTPRoute to add a route to the service created. How does it differ from just an ingress resource?` There's several ways in which it differs, but possibly the key difference is that it has semantics for load-balancing traffic between multiple backends, and backends can be kubernetes Services or something else at the implementors discretion. Also the capabilities for defining TLS are more up front. I would recommend spending some time reading through the website and familiarizing yourself: https://gateway-api.sigs.k8s.io/ |
| | `Will ingress be replaced?` An open question, and it probably depends on who you ask. In the near future it could become the case that a lot of production deployments migrate to HTTPRoute from Ingress. One of my co-maintainers in #sig-network-gateway-api and I both submitted a talk for Kubecon NA this year that would briefly touch on this subject, as this question is a bit of a burning question even for those closest to the project. Ultimately the answer is "maybe" but that really depends on adoption over the coming years.|
| | `How does the deprecated kong-operator compare to the new gateway-operator? Are there any differences in their goal? Or was it just a different implementation?` That old operator was a v0, it was experimental to test the waters and was never intended for production use. We learned quickly that a Helm based operator was limited for what we wanted to do, so we never tried to release a v1 or make it a real supported tool. The new operator is a complete re-envision with real goals for capabilities where the previous one was just an experiment testing the waters.|
| | `Could you also elaborate more on what you mean by managing the life cycle of the ingress controller or proxy?` It may help to read the Kubernetes documentation on Pod lifecycle to get a feel for how the term lifecycle is meant and used, as this term is commonly used in Kubernetes development: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/|
| | Once you have an understanding of what is meant by the lifecycle of a resource like Pod, you can apply it to our ingress-controller and proxy to describe how their states are managed and changed over time during their lifespan. With Helm, the human operator manages these changes in state, mostly. With the operator we're looking to make a lot more of what a human did before significantly more automated. |
| | Once you have an understanding of what is meant by the lifecycle of a resource like Pod, you can apply it to our ingress-controller and proxy to describe how their states are managed and changed over time during their lifespan. With Helm, the human operator manages these changes in state, mostly. With the operator we're looking to make a lot more of what a human did before significantly more automated. |
| | As for the problem you're having, if you wouldn't mind please submitting an issue for that with reproduction steps to the repository we can take a look at that for you (and Slack threads like this tend to get buried anyway, so I haven't been available for the last week and as an individual my availability is not constant, so creating an issue will make it the responsibility of the entire team and also is good for posterity). |

# Credits
* Mentors Ashley and Avery
* Special mentions to Shane on kong slack channel 