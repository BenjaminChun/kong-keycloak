minikube start --insecure-registry 0.0.0.0/0 
eval $(minikube docker-env) 
helm repo add kong https://charts.konghq.com
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install my-release bitnami/postgresql -f ./postgres/values.yaml 
docker build -t kong-oidc:2.4-oidc ./kongv3-dockerfile/
helm install kong/kong --generate-name -f ./kongv4-helm-values/values.yaml
helm install konga ./konga/konga-helm-chart/ -n default -f ./konga/values.yaml
minikube kubectl -- apply -f ./keycloak/deploy-service.yaml